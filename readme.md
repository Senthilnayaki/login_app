# Login Application

This is a simple login application built with Python and Flask.

The login application allows users to sign up for an account, log in, and log out. It provides basic authentication functionality and demonstrates how to handle user sessions in a web application.

## Features

- User registration: Users can sign up for a new account by providing a username and password.

## Requirements

- Python 3.x
- Flask

Access the application in your web browser at 'http://localhost:5000'.
